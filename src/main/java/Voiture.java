import utile.Ut;

public class Voiture {

    // Attributs à définir

    /**
     * Pré-requis : (à compléter)
     * Action : crée une voiture de nom unNom et de vitesse uneVitesse
     * placée à l’origine
     */
    public Voiture(String unNom, int uneVitesse) {
        throw new RuntimeException("Constructeur non implémenté ! Effacez cette ligne et écrivez le code nécessaire");
    }

    /**
     * Résultat : retourne une chaîne de caractères contenant les caractéristiques
     * de this (sous la forme de votre choix)
     */
    public String toString() {
        throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
    }

    /**
     * Résultat : retourne une chaîne de caractères formée d’une suite d’espaces
     * suivie de la première lettre du nom de this, suivie d’un retour
     * à la ligne, le nombre d’espaces étant égal à la position de this.
     */
    public String toString2() {
        throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
    }

    /**
     * Résultat : retourne le nom de this
     */
    public String leNom() {
        throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
    }

    /**
     * Résultat : retourne vrai si et seulement si la position de this est
     * supérieure ou égale à limite
     */
    public boolean depasse(int limite) {
        throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
    }

    /**
     * Pré-requis : (à compléter)
     * Action : fait avancer this d’une distance égale à sa vitesse
     */
    public void avance() {
        throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
    }

    /**
     * Action : place this au départ de la course (à l’origine)
     */
    public void auDepart() {
        throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
    }


}

